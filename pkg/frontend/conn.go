package frontend

import (
	"gitlab.com/akalii/agonesopenmatch/internal/runtime"
	"gitlab.com/akalii/agonesopenmatch/pkg/config"
	"google.golang.org/grpc"
)

func FrontEndConn() (*grpc.ClientConn, error) {
	logger := runtime.Logger()
	omConfig := config.OpenMatch()

	logger.Infof("connecting to OpenMatch FrontEnd service: %s", omConfig.FrontEnd)
	return grpc.Dial(omConfig.FrontEnd, grpc.WithInsecure())
}

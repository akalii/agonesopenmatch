package app

import (
	"context"

	"github.com/sirupsen/logrus"
	"gitlab.com/akalii/agonesopenmatch/internal/runtime"
	"gitlab.com/akalii/agonesopenmatch/pkg/frontend"
	"gitlab.com/akalii/agonesopenmatch/pkg/simulators/players"
)

func RunPlayerSimulator(logger *logrus.Entry, ctx context.Context, interval string, playersPool int) error {
	ctx, cancel := context.WithCancel(context.Background())
	runtime.SetupSignal(cancel)

	conn, err := frontend.FrontEndConn()
	if err != nil {
		logger.Fatal(err)
	}

	feService, err := frontend.NewFrontEndService(conn)
	if err != nil {
		logger.Fatal(err)
	}

	simulator, err := players.NewTimeIntervalPlayerSimulator(interval, playersPool, feService.CreateTicket)
	if err != nil {
		logger.Fatal(err)
	}

	if err := simulator.Run(ctx); err != nil {
		logger.Fatal(err)
	}

	defer conn.Close()

	return nil
}

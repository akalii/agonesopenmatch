FROM golang:1.14 AS builder

WORKDIR /go/src/gitlab.com/akalii/agonesopenmatch

COPY . .

RUN make build && chmod +x /go/src/gitlab.com/akalii/agonesopenmatch/bin/agones-openmatch

FROM alpine

WORKDIR /app

COPY --from=builder /go/src/gitlab.com/akalii/agonesopenmatch/bin/agones-openmatch /app/

ENTRYPOINT ["./agones-openmatch"]

module gitlab.com/akalii/agonesopenmatch

go 1.15

require (
	agones.dev/agones v1.12.0
	github.com/golang/protobuf v1.3.3
	github.com/google/uuid v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	google.golang.org/grpc v1.33.0
	open-match.dev/open-match v1.1.0
)

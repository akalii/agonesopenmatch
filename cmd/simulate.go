package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/akalii/agonesopenmatch/internal/runtime"
	"gitlab.com/akalii/agonesopenmatch/pkg/app"
)

var (
	interval    string
	playersPool int
)

// simulateCmd represents the simulate command
var simulateCmd = &cobra.Command{
	Use:   "simulate",
	Short: "Simulate players requesting matches on a interval basis",
	Long:  `The Player Simulator will request matches on a interval basis for different pool of players.`,
	Run: func(cmd *cobra.Command, args []string) {
		logger := runtime.NewLogger(true).WithField("source", "player_simulator")
		if err := app.RunPlayerSimulator(logger, context.Background(), interval, playersPool); err != nil {
			logger.Fatal(err)
		}
	},
}

func init() {
	playerCmd.AddCommand(simulateCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// simulateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	simulateCmd.Flags().StringVar(&interval, "interval", "5s", "interval between match requests, 10s, 1m, 5m")
	simulateCmd.Flags().IntVar(&playersPool, "players-pool", 10, "number of players to create matchmaking requests")
}

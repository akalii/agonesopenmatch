package cmd

import (
	"context"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/akalii/agonesopenmatch/internal/runtime"
	"gitlab.com/akalii/agonesopenmatch/pkg/config"
	"gitlab.com/akalii/agonesopenmatch/pkg/matchfunction"
)

// functionCmd represents the function command
var functionCmd = &cobra.Command{
	Use:   "mmf",
	Short: "Start the Match Function Server",
	Long: `The Match Function is the component that implements the core matchmaking logic. 
A Match Function receives a MatchProfile as input should return matches for this MatchProfile.`,
	Run: func(cmd *cobra.Command, args []string) {
		logger := runtime.NewLogger(verbose)
		mmfServer, err := matchfunction.NewServer()
		if err != nil {
			logger.Fatal(errors.Wrap(err, "failed to create match function server"))
		}

		ctx, cancel := context.WithCancel(context.Background())
		runtime.SetupSignal(cancel)

		if err := mmfServer.Serve(ctx, config.OpenMatch().MatchFunctionPort); err != nil {
			logger.Fatal(errors.Wrap(err, "failed to start match function server"))
		}
	},
}

func init() {
	rootCmd.AddCommand(functionCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// functionCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// functionCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
